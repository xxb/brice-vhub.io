## Hello World!

Hey! My name's Brice, I'm a Computer Engineer and I love all things computer and technology related. Below is a list of a bunch of things i've done.
Feel free to look around!

 Thanks!

 -Brice 

Stuff
---
  - [Resume (PDF)](https://brice-v.github.io/docs/BriceVadnaisResume.pdf)
  - [Projects](https://brice-v.github.io/projects)
  - Assembly (custom)
    * [My Instruction Set Simulator Documentation](https://brice-v.github.io/Instruction_Set)
  - VHDL
    * [8-bit Microprocessor with 16-bit Data Path](https://github.com/brice-v/8-bit-Custom-Processor) 
  - Python (3)
    * [Microprocessor Virtual Machine/Instruction Set Simulator](https://github.com/brice-v/InstructionSetSimulator-VM)
    * [Microprocessor Assembler](https://github.com/brice-v/Assembler)



## Things I Like

Music is my first and foremost passion.  I play guitar and listen to music every day.  Sometimes I even play live with my friends. I've played the trumpet for 12 years but had to play a little less ever since being in a dorm.

I love to tinker and play with computers, especially single board computers. Recently i've gotten a Raspberry Pi and the possibilites of things that can be done with it are endless.  In addition to that though I've built a plethora of computers and I like to be up to date with the next best thing.  I really like linux and open source software.  My personal favorite distro is Ubuntu for the ease of use.

I'm very interested in microprocessor architecture and decided for my capstone in college that I would build my own.  I learned alot about the core foundations of processor design.  Heres a [link to a instruction set simulator](https://brice-v.github.io/projects#instruction-set-simulator) I built for it!  More recently however I've been looking into the architecture of more complex CPUs such as x86/OpenSparc and more.  The architecture I like the most right now though, is RISC-V because of its open implementation and easy extensibility.  I think this is where the future should head for processor design.

I also like to code! Python has by far become my favorite language becuase of its versatility but I've also coded in Java, Matlab, C/C++, SQL, and Adobe Coldfusion.

Then of course is watching videos, reading books, and playing video games.
Recently I read _Artemis_ by Andy Weir and _Elon Musk: Tesla, SpaceX, and the Quest for a Fantastic Future_ by Ashlee Vance.
I Highly recommend both to anyone interested.

## Achievements
 
  - Magna Cum Laude from University of Hartford
    - Computer Engineering B.S. w/ Computer Science Minor
  - Achieved a Black Belt in American Kenpo Karate after 9 years of study
  - Dean's List: Fall and Spring of 2014, 2015, and 2016 and Spring of 2017
  - President's List: Fall of 2014, 2015 and Spring of 2016
  - All County Jazz Band: Auditioned and played trumpet for 5 years with the Jazz Band
